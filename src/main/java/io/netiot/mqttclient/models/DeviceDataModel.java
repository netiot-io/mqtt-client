package io.netiot.mqttclient.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeviceDataModel {
    private Long deviceId;
    @JsonProperty("data")
    private String data;
    private Long timestamp;
    private Long timestamp_r;
}
