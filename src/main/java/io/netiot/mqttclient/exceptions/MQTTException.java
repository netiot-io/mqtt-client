package io.netiot.mqttclient.exceptions;

public class MQTTException extends RuntimeException {

    public MQTTException() {
    }

    public MQTTException(String message) {
        super(message);
    }
}
