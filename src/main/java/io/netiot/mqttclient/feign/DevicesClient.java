package io.netiot.mqttclient.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "devices")
public interface DevicesClient {
    @RequestMapping(method = RequestMethod.GET, value = "V1/devices/internal/{uuid}/id")
    Long getDeviceId(@PathVariable(name = "uuid") final String uuid);
}
