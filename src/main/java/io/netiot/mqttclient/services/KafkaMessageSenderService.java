package io.netiot.mqttclient.services;

import io.netiot.mqttclient.models.DevicesModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaMessageSenderService {
    @Autowired
    private final KafkaTemplate<String, DevicesModel> kafkaChannel;

    public void sendEvent(final DevicesModel data) {
        log.info("Sending data: " + data.toString());

        kafkaChannel.sendDefault(data);
    }
}
