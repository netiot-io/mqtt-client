package io.netiot.mqttclient.services;

import io.netiot.mqttclient.feign.DevicesClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DevicesService {
    private final DevicesClient devicesClient;

    public Long getDeviceId(final String uuid){
        return devicesClient.getDeviceId(uuid);
    }
}
