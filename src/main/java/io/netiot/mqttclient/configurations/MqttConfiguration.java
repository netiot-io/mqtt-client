package io.netiot.mqttclient.configurations;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netiot.mqttclient.exceptions.MQTTException;
import io.netiot.mqttclient.models.DeviceDataModel;
import io.netiot.mqttclient.models.DevicesModel;
import io.netiot.mqttclient.services.DevicesService;
import io.netiot.mqttclient.services.KafkaMessageSenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.io.IOException;
import java.util.Calendar;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class MqttConfiguration {

    @Value(value = "${mqtt.server}")
    private String server;

    @Value(value = "${mqtt.client-id}")
    private String clientId;

    @Value(value = "${mqtt.topic}")
    private String topic;

    private final DevicesService devicesService;

    private final KafkaMessageSenderService kafkaSender;

    @Bean
    public MessageChannel mqttInputChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageProducer inbound() {
        MqttPahoMessageDrivenChannelAdapter mqttAdapter = new MqttPahoMessageDrivenChannelAdapter(server, clientId, topic);
        mqttAdapter.setCompletionTimeout(5000);
        mqttAdapter.setConverter(new DefaultPahoMessageConverter());
        mqttAdapter.setQos(1);
        mqttAdapter.setOutputChannel(mqttInputChannel());
        return mqttAdapter;
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttInputChannel")
    public MessageHandler handler() {
        return message -> {
            System.out.println(message.getPayload());

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                DevicesModel data = objectMapper.readValue(message.getPayload().toString(), DevicesModel.class);

                String[] subtopics = ((String)message.getHeaders().get("mqtt_receivedTopic")).split("/");
                if (subtopics.length > 1) {
                    String uuid = subtopics[1];
                    Calendar calendar = Calendar.getInstance();
                    long now = calendar.getTimeInMillis();

                    Long id = devicesService.getDeviceId(uuid);

                    for (DeviceDataModel deviceData : data.getDevices()) {
                        deviceData.setDeviceId(id);
                        deviceData.setTimestamp_r(now);
                    }

                    kafkaSender.sendEvent(data);
                }
            } catch (IOException e) {
                throw new MQTTException("Unable to decode mqtt payload!");
            }
        };
    }
}
