MQTT NETIOT client
------------------

Ingests mqtt data into netiot. Any netiot registered device can send data to the mqtt broker to get it into netiot.

Connects automatically to any topic defined in the mqtt section of the config and sends the data to the decoder through
the kafka broker.

It is expected that the subtopic is the device UUID.

The mqtt message must be a json with the following format:
```json
[
    {
      "timestamp": 1571211744187,
      "data": "" 
    }
]
```
where `data` is a string that can be any type of data (bytes, json, etc.) as it will be decoded by the associated decoder.

**Feign**

Communicates with the `Devices` microservice to GET the `deviceId` based on the given `deviceUUID`
```
GET devices/V1/devices/internal/{uuid}/id
returns a LONG value
```

**Kafka**

Every mqtt message received is sent to the kafka broker on the topic definded in the config. The messages sent are a 
list of devices' data:
```json
[
    {
        "deviceId": 1,
        "timestamp": 1571211744187,
        "timestampR": 1571211744188,
        "data": ""
    }
]
``` 
where `data` is a string that can be any type of data (bytes, json, etc.) as it will be decoded by the associated decoder.