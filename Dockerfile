FROM anapsix/alpine-java:8
ADD target/mqtt-client.jar mqtt-client.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /mqtt-client.jar
